all: install_dependencies run_original run_iteration1 run_iteration2
	echo "Complete."

install_dependencies:
	pip install -r requirements.txt
	echo "Dependencies installed."

run_original:
	python3 Original/Original.py
	echo "'Original' done."

run_iteration1:
	python3 Iteration\ 1/Iteration1.py
	echo "'Iteration1' done."

run_iteration2:
	python3 Iteration\ 2/Iteration2.py
	echo "'Iteration2' done."