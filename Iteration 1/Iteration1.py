import requests
import asyncio
import json
import os
from aiohttp import ClientSession
from tqdm import trange
from bs4 import BeautifulSoup

DIR = "Iteration 1/"

def get_urls():
    """
    Gets a dictionary containing each generation populated with urls of the respective Pokémon.
    """
    
    urls = {}
    
    res = requests.get('https://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_by_National_Pok%C3%A9dex_number')
    if res.status_code != 200:
        return {}
    
    soup = BeautifulSoup(res.text, 'html.parser')
    tables = soup.find('div', attrs={'class': 'mw-parser-output'}).find_all('table')
    
    # Remove the tables with no relevant info.
    tables.pop(0)
    tables.pop()
    tables.pop()
    tables.pop()
    
    # Gets the urls of each Pokémon in each generation.
    for i in trange(len(tables), desc='Populating urls for each gen'):
        urls[str(i+1)] = []
        trs = tables[i].tbody.find_all('tr')
        for tr in trs:
            a = tr.th
            if tr.th.find('a') and tr.th.a.find('img'):
                urls[str(i+1)].append(tr.th.a['href'])
    
    return urls


async def fetch(session, url):
    """
    Sends a get request and waits for the response.
    """
    
    async with session.get(url) as response:
        return await response.text()


async def extract_and_save(html):
    """
    Extracts all relevent data from the html response and saves it to a JSON file.
    """
    
    data = {
        "name": None,
        "experienceGroup": None,
        "baseExperienceYieldGen1To4": None,
        "baseExperienceYieldGen5Plus": None,
        "evYields": {
            "hp": None,
            "attack": None,
            "defence": None,
            "specialAttack": None,
            "specialDefence": None,
            "speed": None
        }
    }

    try:
        soup = BeautifulSoup(html, 'html.parser')
        trs = soup.find('table', attrs={'class': 'roundy'}).tbody.findChildren("tr" , recursive=False)
        
        # Name
        data['name'] = trs[0].td.table.tbody.tr.td.table.tbody.tr.td.big.big.b.string.lower()
        
        # I will be using an offset to correct for bulbapedia's inconsistencies between pages.
        # If more than 1 children exist we have an inconsistency.
        offset = 0
        if len(trs[1].findChildren("td" , recursive=False)) > 1:
            offset = 1

        # Base Experience Yield (Gens 5+)
        try:
            data['baseExperienceYield'] = float(trs[7-offset].td.table.tbody.tr.findChildren("td" , recursive=False)[2].get_text().split('V+')[0])
        except:
            # No base experience found
            data['baseExperienceYield'] = -1
        
        # Experience Group
        try:
            data['experienceGroup'] = trs[7-offset].findChildren("td" , recursive=False)[1].table.tbody.tr.td.get_text().replace(' ','').lower()
        except:
            # Experience group is unknown
            data['experienceGroup'] = "none"
            
        # EV Yields
        # Some values have asterisks next to them indicating that there was a change
        # in a previous generation. We will simply remove it and use the current value.
        try:
            tds = trs[8-offset].td.table.tbody.findChildren("tr" , recursive=False)[2].find_all('td')
            data['evYields']['hp'] = int(tds[0].get_text().replace('*', '').split('HP')[0])
            data['evYields']['attack'] = int(tds[1].get_text().replace('*', '').split('Atk')[0])
            data['evYields']['defence'] = int(tds[2].get_text().replace('*', '').split('Def')[0])
            data['evYields']['specialAttack'] = int(tds[3].get_text().replace('*', '').split('Sp')[0])
            data['evYields']['specialDefence'] = int(tds[4].get_text().replace('*', '').split('Sp')[0])
            data['evYields']['speed'] = int(tds[5].get_text().replace('*', '').split('Speed')[0])
        except:
            name = data['name'].lower()
            print(f'There may have been a problem parsing EV Yields for {name}. Consider checking this result manually.')
    
    except Exception as e:
        if data['name'] is not None:
            print('Error for Pokémon ' + data['name'] + ": ")
        print(e)
        return
    
    # Read and concatenate old data with new data.
    concat_data = None
    try:
        with open('species/' + data['name'].lower() + '.json') as f:
            old_data = json.loads(f.read())
            old_data.update(data)
            concat_data = old_data
    except Exception as e:
        print(e)

    # If concatenation was unsuccessful, skip
    if concat_data == None:
        print(f"Skipped {data['name'].lower()}: Error joining old data with new data.")
        return

    # Overwrite file with updated data.
    with open('species/' + data['name'].lower() + '.json', 'w') as f:
        f.write(json.dumps(concat_data, indent=2))
    
    print('Updated and saved to file: ' + data['name'])
    

def concat_urls(urls):
    """
    Concatenate the urls of each generation.
    """
    
    concat = []
    for l in list(urls.values()):
        concat += l
    return concat


async def async_acquire(urls):
    """
    Acquire the data from bulbapedia.
    """
    
    tasks = []
    all_urls = concat_urls(urls)
    async with ClientSession() as session:
        for url in all_urls:
            url = 'https://bulbapedia.bulbagarden.net/' + url
            tasks.append(fetch(session, url))
        htmls = await asyncio.gather(*tasks)
        tasks = []
        for html in htmls:
            tasks.append(extract_and_save(html))
        await asyncio.gather(*tasks)


os.makedirs('species/', exist_ok=True)
urls = get_urls()
asyncio.run(async_acquire(urls))
print('Complete.')