# Original Script

Scrapes the following data from [Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/Main_Page):
* `name`
* `nationalPokedexNumber`
* `primaryType`
* `secondaryType`
* `maleRatio`
* `baseStats`
  * `hp`
  * `attack`
  * `defence`
  * `specialAttack`
  * `specialDefence`
  * `speed`
* `catchRate`