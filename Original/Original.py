import requests
import asyncio
import json
import os
from aiohttp import ClientSession
from tqdm import trange
from bs4 import BeautifulSoup

def get_urls():
    """
    Gets a dictionary containing each generation populated with urls of the respective Pokémon.
    """
    
    urls = {}
    
    res = requests.get('https://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_by_National_Pok%C3%A9dex_number')
    if res.status_code != 200:
        return {}
    
    soup = BeautifulSoup(res.text, 'html.parser')
    tables = soup.find('div', attrs={'class': 'mw-parser-output'}).find_all('table')
    
    # Remove the tables with no relevant info.
    tables.pop(0)
    tables.pop()
    tables.pop()
    tables.pop()
    
    # Gets the urls of each Pokémon in each generation.
    for i in trange(len(tables), desc='Populating urls for each gen'):
        urls[str(i+1)] = []
        trs = tables[i].tbody.find_all('tr')
        for tr in trs:
            a = tr.th
            if tr.th.find('a') and tr.th.a.find('img'):
                urls[str(i+1)].append(tr.th.a['href'])
    
    return urls


async def fetch(session, url):
    """
    Sends a get request and waits for the response.
    """
    
    async with session.get(url) as response:
        return await response.text()


async def extract_and_save(html):
    """
    Extracts all relevent data from the html response and saves it to a JSON file.
    """
    
    data = {
        "name": None,
        "nationalPokedexNumber": None,
        "primaryType": None,
        "secondaryType": None,
        "maleRatio": None,
        "baseStats": {
            "hp": None,
            "attack": None,
            "defence": None,
            "specialAttack": None,
            "specialDefence": None,
            "speed": None
        },
        "catchRate": None,
        "baseScale": None,
        "hitbox": {
            "width": None,
            "height": None,
            "fixed": False
        }
    }

    try:
        soup = BeautifulSoup(html, 'html.parser')
        trs = soup.find('table', attrs={'class': 'roundy'}).tbody.findChildren("tr" , recursive=False)

        # Name
        data['name'] = trs[0].td.table.tbody.tr.td.table.tbody.tr.td.big.big.b.string.lower()

        # Dex
        data['nationalPokedexNumber'] = int(trs[0].td.table.tbody.tr.th.big.big.a.span.string.replace('#', '').lstrip('0'))

        # Type
        temp = trs[1].td.table.tbody.tr.td.table.tbody.tr.find_all('td')
        data['primaryType'] = temp[0].a.span.b.string.lower()
        if len(temp) > 1:
            if temp[1].a.span.b.string.lower() != "unknown":
                data['secondaryType'] = temp[1].a.span.b.string.lower()
            
        # I will be using an offset to correct for bulbapedia's inconsistencies between pages.
        # If more than 1 children exist we have an inconsistency.
        offset = 0
        if len(trs[1].findChildren("td" , recursive=False)) > 1:
            offset = 1

        # Gender Ratio
        try:
            data['maleRatio'] = float(trs[3-offset].td.table.tbody.findChildren("tr" , recursive=False)[1].td.a.span.string.split('%')[0]) / 100
        except:
            # Gender is unkown - ie. Mewtwo.
            data['maleRatio'] = -1
        
        # Catch Rate
        try:
            data['catchRate'] = int(trs[3-offset].findChildren("td" , recursive=False)[1].table.tbody.tr.td.get_text().split()[0])
        except:
            # Catch rate is unknown - ie. Enamorus
            data['catchRate'] = 0
        
        # Base Stats - Requires querying each table due to bulbapedia's inconsistencies.
        for table in soup.find('div', attrs={'class': 'mw-parser-output'}).findChildren("table" , recursive=False):
            try:
                trs = table.tbody.findChildren("tr" , recursive=False)
                data['baseStats']['hp'] = int(trs[2].th.find_all('div')[1].string)
                data['baseStats']['attack'] = int(trs[3].th.find_all('div')[1].string)
                data['baseStats']['defence'] = int(trs[4].th.find_all('div')[1].string)
                data['baseStats']['specialAttack'] = int(trs[5].th.find_all('div')[1].string)
                data['baseStats']['specialDefence'] = int(trs[6].th.find_all('div')[1].string)
                data['baseStats']['speed'] = int(trs[7].th.find_all('div')[1].string)
                break
            except:
                pass
    
    except Exception as e:
        if data['name'] is not None:
            print('Error for Pokémon ' + data['name'] + ": ")
        print(e)
        return
    
    # Write to JSON file.
    with open('species/' + data['name'].lower() + '.json', 'w') as f:
        f.write(json.dumps(data, indent=2))
    print('Saved to file: ' + data['name'])
    

def concat_urls(urls):
    """
    Concatenate the urls of each generation.
    """
    
    concat = []
    for l in list(urls.values()):
        concat += l
    return concat


async def async_acquire(urls):
    """
    Acquire the data from bulbapedia.
    """
    
    tasks = []
    all_urls = concat_urls(urls)
    async with ClientSession() as session:
        for url in all_urls:
            url = 'https://bulbapedia.bulbagarden.net/' + url
            tasks.append(fetch(session, url))
        htmls = await asyncio.gather(*tasks)
        tasks = []
        for html in htmls:
            tasks.append(extract_and_save(html))
        await asyncio.gather(*tasks)


os.makedirs('species/', exist_ok=True)
urls = get_urls()
asyncio.run(async_acquire(urls))
print('Complete.')