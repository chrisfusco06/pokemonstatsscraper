import requests
import asyncio
import json
import os
from aiohttp import ClientSession
from tqdm import trange
from bs4 import BeautifulSoup

DIR = "Iteration 1/"

def get_urls():
    """
    Gets a dictionary containing each generation populated with urls of the respective Pokémon.
    """
    
    urls = {}
    
    res = requests.get('https://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_by_National_Pok%C3%A9dex_number')
    if res.status_code != 200:
        return {}
    
    soup = BeautifulSoup(res.text, 'html.parser')
    tables = soup.find('div', attrs={'class': 'mw-parser-output'}).find_all('table')
    
    # Remove the tables with no relevant info.
    tables.pop(0)
    tables.pop()
    tables.pop()
    tables.pop()
    
    # Gets the urls of each Pokémon in each generation.
    for i in trange(len(tables), desc='Populating urls for each gen'):
        urls[str(i+1)] = []
        trs = tables[i].tbody.find_all('tr')
        for tr in trs:
            a = tr.th
            if tr.th.find('a') and tr.th.a.find('img'):
                urls[str(i+1)].append(tr.th.a['href'])
    
    return urls
    # return {'1': ['wiki/Oshawott_(Pok%C3%A9mon)', 'wiki/Tepig_(Pok%C3%A9mon)']}


async def fetch(session, url):
    """
    Sends a get request and waits for the response.
    """
    
    async with session.get(url) as response:
        return await response.text()

async def fetch(session, url):
    """
    Sends a get request and waits for the response.
    """
    
    async with session.get(url) as response:
        return await response.text()


async def extract_and_save(html, session):
    """
    Extracts all relevent data from the html response and saves it to a JSON file.
    """
    
    data = {
        "name": None,
        "levelUpMoves": {},
        "evoMoves": []
    }

    try:
        soup = BeautifulSoup(html, 'html.parser')
        
        # Name
        data['name'] = soup.find('table', attrs={'class': 'roundy'}).tbody.findChildren("tr" , recursive=False)[0].td.table.tbody.tr.td.table.tbody.tr.td.big.big.b.string.lower()
        
        # Attempts to get the list of moves
        try:
            trs = soup.find('table', attrs={
                    'width': "100%", 
                    'style': "border-collapse:collapse;", 
                    'class': "sortable"
                }).findChildren('tbody', recursive=False)[0].findChildren("tr" , recursive=False)
            while trs[0].th is not None:
                trs.pop(0)
        # There is no gen 8 learnset. Instead get it from different gen (next highest)
        except:
            pokemon_name = data['name']
            for _ in range(7, 0, -1):
                new_url = f'/wiki/{pokemon_name}_(Pok%C3%A9mon)/Generation_VII_learnset'
                try:
                    text = await fetch(session, 'https://bulbapedia.bulbagarden.net/' + new_url)
                    soup = BeautifulSoup(text, 'html.parser')
                    break
                except: pass
            trs = soup.find_all('table')[0].find('table', class_='sortable').findChildren('tbody', recursive=False)[0].findChildren("tr" , recursive=False)
            while trs[0].th is not None:
                trs.pop(0)
        
        # For each move get the level learned and the move name
        for tr in trs:
            try:
                tds = tr.findChildren("td" , recursive=False)
                try:
                    val = [text for text in tds[0].stripped_strings][1]
                except:
                    val = [text for text in tds[0].stripped_strings][0]
                if tds[1].a is not None:
                    move = tds[1].a.string.replace(' ', '').replace('\n,', '').lower()
                else:
                    move = tds[1].string.replace(' ', '').replace('\n,', '').lower()
                
                if not val.isnumeric():
                    if tds[1].a is not None:
                        data['evoMoves'].append(move)
                    else:
                        data['evoMoves'].append(move)
                else:
                    if val not in data['levelUpMoves']:
                        data['levelUpMoves'][val] = []
                    data['levelUpMoves'][val].append(move)
            except Exception as e:
                name = data['name'].lower()
                print(f'There may have been a problem parsing moves for {name}. Consider checking this result manually.')
                print(e)
    
    except Exception as e:
        if data['name'] is not None:
            print('Error for Pokémon ' + data['name'] + ": ")
        print(e)
        return
    
    # Read and concatenate old data with new data.
    concat_data = None
    try:
        with open('species/' + data['name'].lower() + '.json') as f:
            old_data = json.loads(f.read())
            old_data.update(data)
            concat_data = old_data
    except Exception as e:
        print(e)

    # If concatenation was unsuccessful, skip
    if concat_data == None:
        print(f"Skipped {data['name'].lower()}: Error joining old data with new data.")
        return

    # Overwrite file with updated data.
    with open('species/' + data['name'].lower() + '.json', 'w') as f:
        f.write(json.dumps(concat_data, indent=2))
    
    print('Updated and saved to file: ' + data['name'])
    

def concat_urls(urls):
    """
    Concatenate the urls of each generation.
    """
    
    concat = []
    for l in list(urls.values()):
        concat += l
    return concat


async def async_acquire(urls):
    """
    Acquire the data from bulbapedia.
    """
    
    tasks = []
    all_urls = concat_urls(urls)
    async with ClientSession() as session:
        for url in all_urls:
            url = 'https://bulbapedia.bulbagarden.net/' + url
            tasks.append(fetch(session, url))
        htmls = await asyncio.gather(*tasks)
        tasks = []
        for html in htmls:
            tasks.append(extract_and_save(html, session))
        await asyncio.gather(*tasks)


os.makedirs('species/', exist_ok=True)
urls = get_urls()
asyncio.run(async_acquire(urls))
print('Complete.')