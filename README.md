# PokemonStatsScraper

Scrapes Pokémon data from the internet and produces a set of JSON files.

## How To Use

<u>If you have existing species JSONs that you would like to append to:</u>
* Place JSONs in the `species` folder.
* Execute `make install_dependencies`
* Execute `make run_iteration1`
* Execute `make run_iteration2`

<u>If you want a fresh generation of files:</u>
* Execute `make`

<u>If you do not have `make` installed, these are the corresponding commands:</u>
* `make install_dependencies`: `pip install -r requirements.txt`
* `make run_original`: `python3 Original/Original.py`
* `make run_iteration1`: `python3 Iteration\ 1/Iteration1.py`
* `make run_iteration2`: `python3 Iteration\ 2/Iteration2.py`
* `make`: `pip install -r requirements.txt && python3 Original/Original.py && python3 Iteration\ 1/Iteration1.py && python3 Iteration\ 2/Iteration2.py`

## Output
The existing/generated JSONs in the `species` folder will be overwritten with the results.
